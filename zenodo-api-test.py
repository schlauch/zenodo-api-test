"""
Copyright (c) 2018 German Aerospace Center (DLR). All rights reserved.
SPDX-License-Identifier: MIT-DLR

Small test script which showcases the Zenodo REST API.
Please see the README for usage details.
"""


import json
import os

import requests


API_KEY = open("zenodo-api-key.txt", "rb").read() # Requires API key with action and writing permissions
ZENODO_URL = "https://sandbox.zenodo.org/api/deposit/depositions"
DEFAULT_DEPOSITION_METADATA = {
    'metadata': {
         'title': 'TEST -- Example Zenodo API Upload -- TEST',
         'upload_type': 'software',
         'description': 'First try to create and update deposits via Zenodo REST API...',
         'creators': [{'name': 'NAME, FIRST NAME', 'affiliation': 'YOUR AFFILIATION'}]
    }
}


def create_new_deposit():
    """ Creates a new (unpublished) Zenodo deposit and return its deposition ID. """

    headers = {"Content-Type": "application/json"}
    r = requests.post(ZENODO_URL, params={'access_token': API_KEY}, json={}, headers=headers)
    print(r.status_code)
    print(r.json())
    return r.json()["id"]


def set_metadata(deposition_id, metadata):
    """ Sets the given metadata for the specified deposit. """

    headers = {"Content-Type": "application/json"}
    r = requests.put(ZENODO_URL + "/{}".format(deposition_id), params={"access_token": API_KEY}, data=json.dumps(metadata), headers=headers)
    print(r.status_code)
    print(r.json())


def upload_file(deposition_id, file_path):
    """ Uploads a new file for the given deposit. """

    file_name = os.path.basename(file_path)
    data = {"filename": file_name}
    files = {"file": open(file_path, "rb")}
    r = requests.post(ZENODO_URL + "/{}/files".format(deposition_id), params={"access_token": API_KEY}, data=data, files=files)
    print(r.status_code)
    print(r.json())


def publish_deposit(deposition_id):
    """ Publishes the given deposit. BEWARE: It is now visible to all!!! """

    r = requests.post(ZENODO_URL + "/{}/actions/publish".format(deposition_id), params={"access_token": API_KEY})
    print(r.status_code)
    print(r.json())


def create_newversion(deposition_id):
    """ Creates a new version of an already published deposit. """

    r = requests.post(ZENODO_URL + "/{}/actions/newversion".format(deposition_id), params={"access_token": API_KEY})
    print(r.status_code)
    print(r.json())
    return os.path.basename(r.json()["links"]["latest_draft"])


def remove_all_files(deposition_id):
    """ Removes all uploaded files of a unpublished deposit. """

    r = requests.get(ZENODO_URL + "/{}/files".format(deposition_id), params={"access_token": API_KEY})
    print(r.status_code)
    print(r.json())
    for file_entry in r.json():
        print("Remove file entry: " + file_entry["id"])
        print(requests.delete(ZENODO_URL + "/{}/files/{}".format(deposition_id, file_entry["id"]), params={'access_token': API_KEY}))


if __name__ == "__main__":
    # Create a new deposit, upload a ZIP archive, set basic metadata, and publish it.
    deposition_id = create_new_deposit()
    set_metadata(deposition_id, DEFAULT_DEPOSITION_METADATA)
    upload_file(deposition_id, "test-archives/example.zip")
    publish_deposit(deposition_id)
    print("Published new deposit at: {}/{}".format("https://sandbox.zenodo.org/deposit", deposition_id))
    print()

    # Create a new version, remove uploaded file, upload new version, and publish the new version.
    new_version_deposition_id = create_newversion(deposition_id)
    remove_all_files(new_version_deposition_id) # New version contains initial metadata AND uploaded files
    upload_file(new_version_deposition_id, "test-archives/example_V2.zip")
    publish_deposit(new_version_deposition_id)
    print("Published new deposit version at: {}/{}".format("https://sandbox.zenodo.org/deposit", new_version_deposition_id))
